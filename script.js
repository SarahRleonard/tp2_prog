'use strict'


/*constante fixe pour le TP*/
const nom = "Fleury"
const prenom = "Philippe"
const dateNaissance = new Date(1982, 9, 22)
const sexeM = true
const situationParticuliere = false

let consoRecommandeSem = 0
let consoRecommandeJour = 0

/*Nb jour à analyser et consommation*/
let nbJour = parseInt(prompt("Combien de jour voulez-vous analysez?"))
const quantite = []
let nbJourZero = []
let nbJourEx = []

for (let i = 1; i <= nbJour; i++) {
    let consoJour = parseInt(prompt("Quel est le nombre total de consommation d'alcool durant cette période? Jour " + i))

    quantite.push(consoJour)

    if (consoJour == 0) {
        nbJourZero.push(consoJour)
    }
    if (consoJour > consoRecommande().consoRecommandeJour) {
        nbJourEx.push(consoJour)
    }
}

/*Calcul recommander somme*/
function consoRecommande() {
    if (sexeM) {
        consoRecommandeSem = 15
        consoRecommandeJour = 3

    } else {
        consoRecommandeSem = 10
        consoRecommandeJour = 2

    }
    if (situationParticuliere) {
        consoRecommandeSem = 0
        consoRecommandeJour = 0

    }
    return { consoRecommandeSem, consoRecommandeJour }
}

let respect = true

/*date d'aujourd'hui*/
let maintenant = new Date()
maintenant.toLocaleString()
let dateAuj = maintenant.toLocaleString()

console.log(dateAuj)
console.log(nom, prenom)

/*Calcul de l'âge*/
const nbMillisecondes = maintenant - dateNaissance
let age = nbMillisecondes / (1000 * 60 * 60 * 24 * 365.25)
age = Math.floor(age)

console.log("Âge : " + age)

/*Calcul de l'alcool*/
function somme() {
    let somme = 0
    for (let i = 0; i < nbJour; i++) {
        somme = somme + quantite[i]
    }
    return somme
}

console.log("Alcool : " + somme() + " consommation(s)")

/*consommation moyenne par jour*/
const moyenne = somme() / nbJour

console.log("Moyenne par jour : " + moyenne.toFixed(2))

/*Somme hebdo*/
const consoSurUneSem = moyenne * 7

console.log("Consommation sur une semaine : " + somme() + " Recommandation : " + consoRecommande().consoRecommandeSem)

consoRecommandeSem = consoRecommande().consoRecommandeSem
let consoMaxHebdo = Math.max(...quantite)
if (consoMaxHebdo > consoRecommandeSem) { respect = false }

consoRecommandeJour = consoRecommande().consoRecommandeJour
let consoMaxJour = Math.max(...quantite)
if (consoMaxJour > consoRecommandeJour) { respect = false }

console.log("Maximum en une journée : " + consoMaxJour + " Recommandation : " + consoRecommande().consoRecommandeJour)

/*Calcul ratio Excédendaire*/



function ratio(nombre, total) {
    return nombre * 100 / total
}
let ratioExcedentaire = ratio(nbJourEx.length, somme()).toFixed(2)
let ratioJourneeSans = ratio(nbJourZero.length, somme()).toFixed(2)

console.log("Ratio de journées excédants : " + ratioExcedentaire + " %")

/*Calcul ratio Zero*/
console.log("Ratio de journées sans alcool : " + ratioJourneeSans + " %")


if (respect) {
    console.log("Vous respectez les recommandations")
} else {
    console.log("Vous ne respectez pas les recommandations")
}